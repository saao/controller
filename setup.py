from subprocess import call
from setuptools import setup, find_packages
from setuptools.command.install import install as _install
from distutils.command.build import build as _build
from controller import __version__

setup(
    name='Controller',
    description='SAAO Instrumentation Library',
    version=__version__,
    author='Carel van Gend',
    author_email='carel@saao.ac.za',
    license=open('LICENSE').read(),
    packages=find_packages(),
    scripts=[
    ],
    install_requires=[],
    cmdclass={
    },
    data_files=[
    ],
    entry_points={
        'console_scripts': [
            'wincam-top-level-cli = cli.WiNCamCLI:main',
        ],
    }
)
