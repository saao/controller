""" The parent controller.

This is responsible for instantiating each of the child controllers."""

from __future__ import (absolute_import,
                        division,
                        print_function,
                        unicode_literals)

import logging as log

from lesedi.client import ObservatoryClient as LesediClient

import time
from datetime import datetime
import sys

class LesediController:
    def __init__(self,
                 remote_host="localhost",
                 lesedi_port=9095,
                 telescope="1m"):
        self.remote_host = remote_host
        self.lesedi_port = lesedi_port
        self.telescope = telescope

    @property
    def lesedic(self):
        if not getattr(self, '_lesedic', False):
            try:
                self._lesedic = lesediClient(
                    host=self.remote_host)
            except:
                self._lesedic = {}
        return self._lesedic

if __name__ == '__main__':
    print("Instantiating  parent controller")
    try:
        controller = LesediController()
    except:
        print("Exception while instantiating controller")
        sys.exit()
    try:
        print("Getting statuses")
        print("")
        print("lesedi status: {}".format(controller.lesedic.get_status()))
        print("")
    except:
        print("Caught an exception getting statuses")
    print("...and we're done")
