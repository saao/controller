from weather.client.weather_client import WeatherClient
from lesedi.sdk.client import Client as TCSClient
from collections import namedtuple
from threading import Thread
from datetime import datetime
import time
import threading
import shutil
import os
import json
import logging as log
import time
instrument = "wincam"


if instrument == 'wincam':
    from wincam.controller import Controller as InstrumentClient
    from wincam import detector
    from wincam.detector.interface.ttypes import WincamDetectorException as InstrumentDetectorException
    from wincam.shutter.interface.ttypes import WincamShutterException as InstrumentShutterException
    from wincam.filter.interface.ttypes import WincamFilterException as InstrumentFilterException
    from wincam.cryostat.interface.ttypes import WincamCryostatException as InstrumentCryostatException
    from wincam.interface.ttypes import WincamException as InstrumentException
else:
    print("Instrument type not defined or supported")
    raise Exception("Instrument type not defined")


LONGEST_READOUT_TIME = 900
FITS_INFO_GATHER_TIME = 2
system_state = namedtuple(
    "system_state", "instrument_state tcs_state weather_state")


class Controller(object):

    def __init__(self,
                 weatherhost='localhost',
                 tcshost='localhost',
                 instrumenthost='localhost',
                 connections=None):
        self.instrumenthost = instrumenthost
        self.weatherhost = weatherhost
        self.tcshost = tcshost
        self._connections = connections or threading.local()
        self.error = None
        self.script_aborted = False

    @property
    def instrument(self):
        if not hasattr(self, '_instrument'):
            self._instrument = InstrumentClient(connections=self._connections)
        return self._instrument

    @property
    def tcs(self):
        if not hasattr(self._connections, 'tcs'):
            self._connections.tcs = TCSClient(host=self.tcshost)
        return self._connections.tcs

    @property
    def weather(self):
        if not hasattr(self._connections, 'weather'):
            self._connections.weather\
                = WeatherClient(tcp_host=self.weatherhost)
        return self._connections.weather

    def state(self):
        if self.error is not None:
            message = self.error
            self.error = None
            log.error(".....Caught exception in thread...{}".format(message))
            raise InstrumentException(message)
        try:
            instrument_state = self.instrument.state()
        except (InstrumentDetectorException,
                InstrumentFilterException,
                InstrumentShutterException,
                InstrumentCryostatException) as e:
            message = e.message
            log.error("Caught exception getting state: {}".format(message))
            raise e

        tcs_state = None  # TBD - get this
        weather_state = None  # TBD - get this
        return system_state(instrument_state, tcs_state, weather_state)

    def abort_exposure(self):
        self.aborted = True
        self.instrument.detector.abort_exposure()

    def start_exposure(self, fits_info={}, is_continuous=False, initial_start_time=0):
        self.aborted = False
        log.info("Called start_exposure() in TopLevelController with continuous = {}".format(
            is_continuous))
        self.current_error = None
        try:
            self.instrument.is_ready()
        except (InstrumentDetectorException,
                InstrumentFilterException,
                InstrumentShutterException,
                InstrumentCryostatException) as e:
            msg = "Exposure not started: {}".format(e)
            log.error(msg)
            raise e
        start_exposure_worker = Thread(name='start_exposure_worker',
                                       target=self.start_exposure_worker,
                                       args=(fits_info, is_continuous, initial_start_time))
        log.debug("Starting thread to handle exposures")
        start_exposure_worker.start()

        time.sleep(2)
        if self.error is not None:
            message = self.error
            self.error = None
            log.error("Error starting exposure: {}".format(message))
            raise InstrumentException(message)

    def start_exposure_worker(self, fits_info={}, is_continuous=False, initial_start_time=0):
        log.info(
            "Starting exposure worker thread with continuous = {}".format(is_continuous))
        log.debug("Worker thread: setting up exposures")

        try:
            self.instrument.detector.setup_exposure(self)
        except (InstrumentDetectorException,
                InstrumentFilterException,
                InstrumentShutterException,
                InstrumentCryostatException) as e:
            errmsg = "Caught detector exception setting up exposure: {}".format(
                e)
            if hasattr(e, 'message'):
                errmsg = "Error while setting up exposure: {}".format(
                    e.message)
                log.error(errmsg)
                self.error = errmsg
                return
        except Exception as e:
            errmsg = "Caught exception setting up exposure: {}".format(e)
            log.error(errmsg)
            self.error = errmsg
            return

        detector_state = self.instrument.detector.get_state()
        num_exposures = detector_state.num_exposures
        if is_continuous:
            num_exposures = detector_state.num_previews
        exposure_time = detector_state.exposure_time
        log.debug("Worker thread: doing {} exposures".format(num_exposures))

        # Get the initial set of fits info
        call_gather_time = time.time()
        if not is_continuous:
            fits_info = self.gather_fits_info(fits_info)
            log.debug("Time to gather fits info was {}".format(
                time.time() - call_gather_time))

        # Generate a list of the readout times that would be required
        # in FT mode. If we're in CCD, we're just using the list to
        # iterate over exposures. If we're in FTCCD mode, these times
        # are used to determine the next readout.

        # TBD: Allow for initial_start_time to be a real future time (i.e. not 0)
        # TBD: Sleep until initial start time
        now = time.time()
        readout_times = [now + i * exposure_time
                         for i in range(num_exposures)]

        old_ro = "0"
        for exposure_number, current_readout_time in enumerate(readout_times, start=1):
            detector_state = self.instrument.detector.get_state()
            if self.aborted:
                self.aborted = False
                break
            start_time = time.time()
            log.debug("Start of exposure iteration {}. Time = {}".format(
                exposure_number, current_readout_time))
            log.debug("Actual start time = {}".format(start_time))

            dateobs = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
            if detector_state.detector_type == detector.ttypes.DetectorType.FTCCD:
                fits_info["DATE-OBS"] = {
                    old_ro: "The time the exposure was started (UTC)"}
                fits_info["DATE-RO"] = {
                    dateobs: "The time the readout was triggered (UTC)"}
            else:
                fits_info["DATE-OBS"] = {
                    dateobs: "The time the exposure was started (UTC)"}
            old_ro = dateobs
            try:
                self.instrument.detector.start_exposure(fits_info, is_continuous)
            except InstrumentDetectorException as e:
                errmsg = "Caught detector exception starting exposure: {}".format(e)
                if hasattr(e, 'message'):
                    errmsg = "Error while starting exposure: {}".format(e.message)
                log.error(errmsg)
                self.error = errmsg
                return

            # Abort may have been called after start_exposure was called and before it returned
            if self.aborted:
                self.aborted = False
                break

            if detector_state.detector_type == detector.ttypes.DetectorType.FTCCD:
                log.debug("Readout {} of {} triggered".
                          format(exposure_number, num_exposures))
                # Figure out how long to wait before the next exposure
                if not is_continuous:
                    sleep_time = current_readout_time + exposure_time \
                        - time.time() - FITS_INFO_GATHER_TIME
                    log.debug("Sleep till fits gathering = {}    Current time = {}".format(
                        sleep_time, time.time()))
                    # Do we still have time to gather fits info?
                    if sleep_time < 0:
                        errmsg = "Not enough time to gather fits info and trigger readout on time"
                        log.error(errmsg)
                        self.error = errmsg
                        return
                    # Sleep till we must gather fits info
                    while sleep_time > 1:
                        # Wake every second to see if we're aborted
                        # or if the exposure sequence is complete
                        time.sleep(1)
                        if self.sequence_complete:
                            log.debug("Sequence is complete. Exiting FT thread")
                            self.exposure_thread_exited = True;
                            return

                        if self.aborted:
                            log.debug("Abort condition detected")
                            self.aborted = False
                            return
                        sleep_time -= 1
                        log.debug("zzz...Woke up. Remaining time is {}...zzz".format(sleep_time))
                    # Sleep off the last bit
                    log.debug("Doing last little sleep of {}".format(sleep_time))
                    time.sleep(sleep_time)
                    # Gather the fits info
                    call_gather_time = time.time()
                    fits_info = self.gather_fits_info(fits_info)
                    log.debug("Time to gather fits info was {}".format(
                        time.time() - call_gather_time))

                # Work out when to take the exposure
                remaining_time = current_readout_time + exposure_time - time.time()
                log.debug("Remaining time = {}    Current time = {}".format(
                    remaining_time, time.time()))
                # Have we passed the requested exposure time?
                if remaining_time < 0:
                    errmsg = "Last exposure took longer than the requested readout time"
                    log.error(errmsg)
                    self.error = errmsg
                    return

                # Otherwise, sleep till it's time to gather the next fits info
                time.sleep(remaining_time)
                log.debug("Woke up. Current time = {}".format(time.time()))
                # Sanity check: Check that the readout has completed
                detector_state = self.instrument.detector.get_state()
                if detector_state.exposure_state not in [
                        detector.ttypes.ExposureState.COMPLETE,
                        detector.ttypes.ExposureState.IDLE,
                        detector.ttypes.ExposureState.PROCESSING]:
                    errmsg = "Attempted to start next exposure before readout complete."
                    log.error(errmsg)
                    self.error = errmsg
                    return

                continue  # To next FTCCD exposure

            # If we're here, we're in full frame mode
            num_tries = 0
            MAX_TRIES = detector_state.exposure_time + LONGEST_READOUT_TIME
            while True:
                num_tries += 1
                detector_state = self.instrument.detector.get_state()
                if (detector_state.exposure_state ==
                        detector.ttypes.ExposureState.IDLE):
                    log.debug("Exposure {} of {} done".
                              format(exposure_number, num_exposures))
                    log.info("All exposures done")
                    return
                if (detector_state.exposure_state ==
                        detector.ttypes.ExposureState.COMPLETE):
                    log.debug("Exposure {} of {} done".
                              format(exposure_number, num_exposures))
                    break
                if num_tries > MAX_TRIES:
                    log.error("Exposure is taking too long. Giving up")
                    self.instrument.detector.abort_exposure()
                    return
                time.sleep(1)

            # We're done exposing. Do the next
            fits_info = self.gather_fits_info(fits_info)
            # Continue with the next exposure

        self.exposure_thread_exited = True;
        log.debug("Start exposure worker exiting")

    def gather_fits_info(self, fits_info={}):
        # try:
        #     weather_info = self.weather.gather_fits_info()
        #     fits_info.update(weather_info)
        # except Exception as e:
        #     msg = "Could not retrieve weather fits info: {}".format(e)
        #     log.error(msg)
        # try:
        #     tcs_info = self.tcs.gather_fits_info()
        #     fits_info.update(tcs_info)
        # except Exception as e:
        #     msg = "Could not retrieve TCS fits info: {}".format(e)
        #     log.error(msg)
        try:
            instrument_info = self.instrument.gather_fits_info()
            fits_info.update(instrument_info)
        except Exception as e:
            msg = "Could not retrieve instrument fits info: {}".format(e)
            log.error(msg)
        return fits_info

    def import_script(self, scriptfile):
        with open(scriptfile) as f:
            script = json.load(f)
        return script

    def run_script(self, script, iterations=1):
        script_name = script.get("script_name", "Unnamed script")
        log.info("Running script {}".format(script_name))
        worker = Thread(name='worker',
                        target=self.script_worker,
                        args=(script, iterations))
        worker.start()

    def abort_script(self):
        self.script_aborted = True
        self.instrument.detector.abort_exposure()

    def script_worker(self, script, iterations):
        """Control the repeats and iteration through a script."""

        self.script_info = {}
        self.script_info["active"] = True
        self.script_info["total_iterations"] = iterations

        log.debug("In script worker thread")
        # Repeat the whole script the specified number of times
        for iteration in range(iterations):
            if self.script_aborted:
                self.script_aborted = False
                return
            self.script_info["current_iteration"] = iteration
            log.info(
                "Doing iteration {} of {}".format(iteration + 1, iterations)
            )
            # The script is a list of single script units, which we
            # process one at a time.
            self.script_info["total_units"] = len(script["script_units"])

            # Create a temp directory to store the parameter files
            timestamp = time.strftime("%Y-%m-%dH%H:%M:%S")
            dirname = "tmp_{}".format(timestamp)
            os.mkdir(dirname)

            # Step through each of the script units
            for script_unit_count, script_unit in \
                    enumerate(script["script_units"]):
                # Stop if the user aborted the script
                if self.script_aborted:
                    self.script_aborted = False
                    return
                self.script_info["script_unit_count"] = script_unit_count
                log.info("Running script unit {} of {}"
                         .format(script_unit_count + 1,
                                 len(script["script_units"])))

                if "comment" in script_unit:
                    self.script_info["comment"] = script_unit["comment"]
                else:
                    self.script_info["comment"] = None

                # Process each script unit
                self.stage_run_script_unit(script_unit, script_unit_count)

                # Copy the parameter file to a tmp directory
                paramsrc = "/home/ccd/IDSAC_PARAMETERS"
                shutil.copyfile("{}/ParameterFile.txt".format(paramsrc),
                                "{}/ParameterFile_{:0>3d}.txt"
                                .format(dirname, script_unit_count))
                time.sleep(2)
                log.info("Script unit {} of {} done"
                         .format(script_unit_count + 1,
                                 len(script["script_units"])))

            log.info("All {} script units complete"
                     .format(len(script["script_units"])))

        log.info("All iterations of script done")
        self.script_info["active"] = False

    def stage_run_script_unit(self, script_unit, script_unit_count):
        """ Extract the various sections, stage each and then take the exposure"""

        # Sanity check: don't try to start a new script unit if the last exposure thread has not finished exiting
        tries_remaining = 5
        try:
            while (not self.exposure_thread_exited):
                if tries_remaining <= 0:
                    log.error("Trying to start script unit before previous exposure finished")
                    return
                tries_remaining -= 1
                time.sleep(1)
        except:
            pass

        # The telescope section is executed first (if there is one)
        if "TCS" in script_unit:
            log.info("Staging TCS")
            self.script_info["staging_tcs"] = True
            telscript = script_unit["TCS"]
            try:
                self.tcs.run_script(telscript)
            except Exception as e:
                self.script_info["staging_tcs"] = False
                log.error(
                    "Script: Error moving telescope to position"
                    "for script unit {}: {}"
                    .format(script_unit_count+1, e))
                # There's no point in continuing with this script unit
                return
            log.info("Staging TCS done")
            self.script_info["staging_tcs"] = False

        # We can stop if there's no instrument section
        if "instrument" not in script_unit:
            return

        # If we're here, then there is an instrument to be staged
        log.info(
            "Staging instrument for script {}"
            .format(script_unit_count+1)
        )
        self.script_info["staging_instrument"] = True
        instscript = script_unit["instrument"]
        try:
            self.instrument.stage_script(instscript)
        except Exception as e:
            self.script_info["staging_instrument"] = False
            log.error("Script: Error staging instrument"
                      " for script unit {}: {}"
                      .format(script_unit_count + 1, e))
            # There's no point in continuing with this script unit
            return   # Do the next unit in the script

        # TBD: Check that telescope and any other components are ready
        # Check that all instrument components are ready
        num_tries = 0
        MAX_TRIES = 20
        while (True):
            try:
                self.instrument.is_ready()
                break
            except Exception as e:
                log.info("Instrument not ready: {}".format(e))
                num_tries += 1
                if num_tries > MAX_TRIES:
                    log.error("Instrument setup timeout for script unit {}"
                              .format(script_unit_count))
                    return
                time.sleep(1)

        # Everything is ready. Take the exposures!

        log.info("Done staging instrument for script unit {}"
                 .format(script_unit_count + 1))
        self.script_info["staging_instrument"] = False

        # If we're here, everything was correctly staged.

        fits_info = {}
        fits_info['SCRIPTNO'] = {
            "{}".format(script_unit_count+1):
            "The number in the sequence of script units"
        }
        # Go!
        log.info(
            "Starting exposures for script unit {}"
            .format(script_unit_count+1))
        try:
            self.sequence_complete = False
            self.start_exposure(fits_info)
            log.debug("Waiting for sequence to finish")
            self.instrument.detector.wait_for_sequence_finish(3, 1)
            self.sequence_complete = True
            log.debug(
                "Done exposures for script unit {}"
                .format(script_unit_count+1)
            )
        except Exception as e:
            log.error("Error taking exposure"
                      " for script unit {}: {}"
                      .format(script_unit_count + 1, e))
            return
        log.info(
            "Completed script unit {}"
            .format(script_unit_count + 1)
        )
        # All done


if __name__ == '__main__':
    tlc = Controller(instrumenthost='localhost',
                     weatherhost='localhost',
                     tcshost='localhost')
    fits_info = tlc.gather_fits_info()
    for k, v in fits_info.items():
        vk, vv = v.items()[0]
        print("{}: {} # {}".format(k, vk, vv))

    script = tlc.import_script("example.json")
    tlc.run_script(script)
