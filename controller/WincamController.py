""" The parent controller.

This is responsible for instantiating each of the child controllers."""

from __future__ import (absolute_import,
                        division,
                        print_function,
                        unicode_literals)

import logging as log

from wincam.detector.client import Client as DetectorClient
# from wincam.filter.client import Client as FilterClient
from wincam.shutter.client import Client as ShutterClient
from wincam.cooler.client import Client as CoolerClient
#from wincam.temperature_controller.client import Client as TemperatureControllerClient

from lesedi.client import ObservatoryClient as LesediClient

import time
from datetime import datetime
import sys

class WincamController:
    def __init__(self,
                 detector_host="localhost",
                 detector_port=9083,
                 filter_host="localhost",
                 filter_port=9084,
                 shutter_host="localhost",
                 shutter_port=9082,
                 cooler_host="localhost",
                 cooler_port=9081,
                 temperature_controller_host="localhost",
                 temperature_controller_port=9080,
                 lesedi_host="localhost",
                 lesedi_port=9050
    ):
        self.detector_host = detector_host
        self.detector_port = detector_port
        self.filter_host = filter_host
        self.filter_port = filter_port
        self.shutter_host = shutter_host
        self.shutter_port = shutter_port
        self.cooler_host = cooler_host
        self.cooler_port = cooler_port
        self.temperature_controller_host = temperature_controller_host
        self.temperature_controller_port = temperature_controller_port
        self.lesedi_host = lesedi_host
        self.lesedi_port = lesedi_port



    @property
    def detectorc(self):
        if not getattr(self, '_detectorc', False):
            try:
                self._detectorc = DetectorClient(
                    host=self.detector_host,
                    port=self.detector_port
            )
            except:
                self._detectorc = {}
        return self._detectorc

    @property
    def filterc(self):
        if not getattr(self, '_filterc', False):
            try:
                self._filterc = FilterClient(
                    host=self.remote_host)
            except:
                self._filterc = {}
        return self._filterc

    @property
    def shutterc(self):
        if not getattr(self, '_shutterc', False):
            try:
                self._shutterc = ShutterClient(
                    host=self.remote_host)
            except:
                self._shutterc = {}
        return self._shutterc

    @property
    def coolerc(self):
        if not getattr(self, '_coolerc', False):
            try:
                self._coolerc = CoolerClient(
                    host=self.remote_host)
            except:
                self._coolerc = {}
        return self._coolerc

    @property
    def temperature_controllerc(self):
        if not getattr(self, '_temperature_controllerc', False):
            try:
                self._temperature_controllerc = Temperature_ControllerClient(
                    host=self.remote_host)
            except:
                self._temperature_controllerc = {}
        return self._temperature_controllerc

    @property
    def lesedic(self):
        if not getattr(self, '_lesedic', False):
            try:
                print("Instantiating LesediClient with {} {}".format(self.lesedi_host, self.lesedi_port))
                self._lesedic = LesediClient(
                    host=self.lesedi_host,
                    port=self.lesedi_port
                )
                print("Successfully instantiated LesediClient")
            except:
                print("Failed to instantiate LesediClient. Using empty dict")
                self._lesedic = {}
        return self._lesedic

    def start_exposure(self, fits_info = {}):
        self.get_filter_info(fits_info)
        self.get_tcs_info(fits_info)
        self.get_weather_info(fits_info)
        self.get_timing_info(fits_info)
        self.detectorc.start_exposure(fits_info)

    def get_filter_info(self, fits_info):
        # Get the filter
        fits_info["FILTER"] = {"": "The active filter"}

    def get_tcs_info(self, fits_info):
        # Get the TCS information 
        fits_info["TELRA"] = {"NA": "The telescope right ascension"}
        fits_info["TELDEC"] = {"NA": "The telescope declination"}
        fits_info["AIRMASS"] = {"NA": "The airmass (sec(z))"}
        fits_info["ZD"] = {"NA": "The telescope zenith distance"}
        fits_info["HA"] = {"NA": "The telescope hour angle"}
        fits_info["TELFOCUS"] = {"NA": "The telescope focus"}
        fits_info["INSTANGL"] = {"NA": "The instrument angle"}
        fits_info["DOMEPOS"] = {"NA": "The dome position"}
        try:
            tcs = self.lesedic.infoMount()
            if len(tcs) > 0:
                fits_info["TELRA"] = {
                    str(tcs["tel_ra"]): "The telescope right ascension"}
                fits_info["TELDEC"] = {
                    str(tcs["tel_dec"]): "The telescope declination"}
                fits_info["AIRMASS"] = {
                    str(tcs["airmass"]): "The airmass (sec(z))"}
                fits_info["ZD"] = {
                    str(tcs["zenithdistance"]): "The telescope zenith distance"}
#                fits_info["HA"] = {tcs["HA"]: "The telescope hour angle"}
                fits_info["TELFOCUS"] = {str(tcs["focus"]): "The telescope focus"}
        except Exception as e:
            log.error("Unable to get TCS info: {}".format(str(e)))
        try:
            domeinfo = self.lesedic.infoDome()
            if len(domeinfo) > 0:
                fits_info["DOMEPOS"] = {
                    str(domeinfo["domeangle"]): "The dome position"}
        except Exception as e:
            log.error("Unable to get TCS info: {}".format(str(e)))
        
    def get_weather_info(self, fits_info):
        # Get the weather and seeing information
        fits_info["TMTDEW"] = {"NA": "Difference between current and dew-point temperatures"}
        fits_info["HUMIDIT"] = {"NA": "The average humidity"}
        fits_info["RELSKYT"] = {"NA": "The relative sky temperature (average cloud cover)"}
        fits_info["WIND"] = {"NA": "The average wind speed"}
        fits_info["ENVTEM"] = {"NA": "The average ambient temperature"}
        fits_info["SEEING"] = {"NA": "The curent seeing"}
        # try:
        #     weather_info = self.weathc.get_weather_info()
        #     if len(weather_info) > 0:
        #         fits_info["TMTDEW"] = {weather_info["avg_t_min_tdew"]: "Difference between current and dew-point temperatures"}
        #         fits_info["HUMIDIT"] = {weather_info["avg_hum"]: "The average humidity"}
        #         fits_info["RELSKYT"] = {weather_info["avg_cloud"]: "The relative sky temperature (average cloud cover)"}
        #         fits_info["WIND"] = {weather_info["avg_wind"]: "The average wind speed"}
        #         fits_info["ENVTEM"] = {weather_info["avg_temp"]: "The average ambient temperature"}
        #         fits_info["SEEING"] = {weather_info["seeing"]: "The curent seeing"}
        # except Exception as e:
        #     log.warn("Unable to get weather and seeing information: {}".format(e))

    def get_timing_info(self, fits_info):
        fits_info["DATE-OBS"] = {datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f"): "The time the user pushed start (UTC)"}

if __name__ == '__main__':
    print("Instantiating  parent controller")
    try:
        controller = WincamController()
    except:
        print("Exception while instantiating controller")
        sys.exit()
    try:
        print("Getting statuses")
        print("")
        print("Wincam detector state: {}".format(controller.detectorc.get_state()))
        print("")
    except:
        print("Caught an exception getting statuses")
    print("...and we're done")
