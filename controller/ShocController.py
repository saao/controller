""" The parent controller.

This is responsible for instantiating each of the child controllers."""

from __future__ import (absolute_import,
                        division,
                        print_function,
                        unicode_literals)

import logging as log

from shoc.filterwheel.client import Client as FilterWheelClient
from shoc.gps.client import Client as GPSClient
from shoc.controller.camera_controller import CameraController
try:
    from tcsview.client.tcsview_client import TCSViewClient
except ImportError as e:
    log.warn("No such module: {}".format(e))

try:
    from rts2view.client.rts2view_client import RTS2ViewClient
except ImportError as e:
    log.warn("No such module: {}".format(e))

try:
    from weather.client.weather_client import WeatherClient
except ImportError as e: 
    log.warn("No such module: {}".format(e))
import time
from datetime import datetime
import sys
import paramiko

class Controller:
    def __init__(self,
                 remote_host="localhost",
                 tcsview_host="localhost",
                 filterwheel_port=9090,
                 gps_port=9091,
                 camera_port=9092,
                 tcsview_port=9093,
                 weather_port=9094,
                 telescope="74in"):
        self.remote_host = remote_host
        self.filterwheel_port = filterwheel_port
        self.gps_port = gps_port
        self.camera_port = camera_port
        self.tcsview_port = tcsview_port
        self.weather_port = weather_port
        self.telescope = telescope

    @property
    def fwc(self):
        if not getattr(self, '_filterwheel', False):
            try:
                self._filterwheel = FilterWheelClient(
                    host=self.remote_host, port=self.filterwheel_port)
            except:
                self._filterwheel = {}
        return self._filterwheel

    @property
    def gpsc(self):
        if not getattr(self, '_gps', False):
            self._gps = GPSClient(
                host=self.remote_host, port=self.gps_port)
        return self._gps

    @property
    def camc(self):
        if not getattr(self, '_camc', False):
            self._camc = CameraController(
                tcp_host=self.remote_host, tcp_port=self.camera_port, test_mode=False)
        return self._camc

    @property
    def tcsvc(self):
        if not getattr(self, '_tcsvc', False):
            log.info("Loading TCS view module for {}".format(self.telescope))
            if self.telescope == "74in":
                self._tcsvc = TCSViewClient(
                    tcp_host=self.remote_host, tcp_port=self.tcsview_port)
            elif self.telescope == "1m":
                self.tcsview_host = "1ms1.suth.saao.ac.za"
                self._tcsvc = RTS2ViewClient(
                    tcp_host=self.tcsview_host, tcp_port=self.tcsview_port)
            else:
                log.warn("No such telescope")

        return self._tcsvc

    @property
    def weathc(self):
        if not getattr(self, '_weathc', False):
            self._weathc = WeatherClient(
                tcp_host=self.remote_host, tcp_port=self.weather_port)
        return self._weathc

    """Start the exposure.
    
    The supplemental parameters can be passed in as a dictionary of strings (FITS keywords) mapping to a dictionary of {value: comment}.
    The filter, POP, TCS and weather information are deduced.
"""
    def start_exposure(self, fits_info = {}):
        self.get_filter_info(fits_info)
        self.get_tcs_info(fits_info)
        self.get_weather_info(fits_info)
        self.get_timing_info(fits_info)
        self.camc.start_exposure(fits_info)

    def get_filter_info(self, fits_info):
        # Get the filter
        fits_info["FILTERA"] = {"": "The active filter in wheel A"}
        fits_info["FILTERB"] = {"": "The active filter in wheel B"}
        fits_info["WHEELA"] = {"": "The ID of wheel A"}
        fits_info["WHEELB"] = {"": "The ID of wheel B"}
        fits_info["POSA"] = {"": "The slot position of wheel A"}
        fits_info["POSB"] = {"": "The slot position of wheel B"}

        for wheel in self.fwc.wheels.values():
            wheel_id = wheel.wheel_id
            filter_name = 'None'
            if wheel_id != '000' and wheel_id != 0:
                wheel_pos = wheel.position - 1
                try:
                    filter_name, filter_color = wheel.filters[wheel_pos]
                except KeyError:
                    pass
                fits_keyword = 'FILTER{}'.format(wheel.name)
                fits_info[fits_keyword] = {str(filter_name):
                                           'The active filter in wheel {}'.format(wheel.name)}
                fits_keyword = 'WHEEL{}'.format(wheel.name)
                fits_info[fits_keyword] = {'{0:b}'.format(int(wheel_id)):
                                           'The ID of wheel {}'.format(wheel.name)}
                fits_keyword = 'POS{}'.format(wheel.name)
                fits_info[fits_keyword] = {str(wheel.position):
                                           'The slot position of wheel {}'.format(wheel.name)}

    def get_tcs_info(self, fits_info):
        # Get the TCS information 
        fits_info["TELRA"] = {"NA": "The telescope right ascension"}
        fits_info["TELDEC"] = {"NA": "The telescope declination"}
        fits_info["AIRMASS"] = {"NA": "The airmass (sec(z))"}
        fits_info["ZD"] = {"NA": "The telescope zenith distance"}
        fits_info["HA"] = {"NA": "The telescope hour angle"}
        fits_info["TELFOCUS"] = {"NA": "The telescope focus"}
        fits_info["INSTANGL"] = {"NA": "The instrument angle"}
        fits_info["DOMEPOS"] = {"NA": "The dome position"}
        if self.telescope == "74in":
            try:
                tcs = self.tcsvc.get_tcs_info()
                if len(tcs) > 0:
                    fits_info["TELRA"] = {tcs["TELRA"]: "The telescope right ascension"}
                    fits_info["TELDEC"] = {tcs["TELDEC"]: "The telescope declination"}
                    fits_info["AIRMASS"] = {tcs["AIRMASS"]: "The airmass (sec(z))"}
                    fits_info["ZD"] = {tcs["ZD"]: "The telescope zenith distance"}
                    fits_info["HA"] = {tcs["HA"]: "The telescope hour angle"}
                    fits_info["TELFOCUS"] = {tcs["TELFOCUS"]: "The telescope focus"}
                    fits_info["INSTANGL"] = {tcs["INSTANGL"]: "The instrument angle"}
                    fits_info["DOMEPOS"] = {tcs["DOMEPOS"]: "The dome position"}
            except Exception as e:
                log.error("Unable to get TCS info for telescope {}: {}".format(self.telescope, str(e)))
        elif self.telescope == "1m":
            log.info("Getting tcs info for 1m")
            try:
                mi = self.tcsvc.infoMount()
                fits_info["TELRA"] = {str(mi.TEL.ra): "The telescope right ascension"}
                fits_info["TELDEC"] = {str(mi.TEL.dec): "The telescope declination"}
                fits_info["ALT"] = {str(mi.HRZ.alt): "The telescope altitude"}
                fits_info["AZ"] = {str(mi.HRZ.az): "The telescope azimuth"}
                fits_info["OFFSRA"] = {str(mi.offsets.ra): "The RA offset"}
                fits_info["OFFSDEC"] = {str(mi.offsets.ra): "The dec offset"}
                fits_info["TARGRA"] = {str(mi.ORI.ra): "The target RA"}
                fits_info["TARGDEC"] = {str(mi.ORI.dec): "The target dec"}
            except Exception as e:
                log.error("Unable to get TCS info for telescope {}: {}".format(self.telescope, str(e)))
        else:
            log.info("TCS information not available for telescope {}".format(self.telescope))
        
    def get_weather_info(self, fits_info):
        # Get the weather and seeing information
        fits_info["TMTDEW"] = {"NA": "Difference between current and dew-point temperatures"}
        fits_info["HUMIDIT"] = {"NA": "The average humidity"}
        fits_info["RELSKYT"] = {"NA": "The relative sky temperature (average cloud cover)"}
        fits_info["WIND"] = {"NA": "The average wind speed"}
        fits_info["ENVTEM"] = {"NA": "The average ambient temperature"}
        fits_info["SEEING"] = {"NA": "The curent seeing"}
        try:
            weather_info = self.weathc.get_weather_info()
            if len(weather_info) > 0:
                fits_info["TMTDEW"] = {weather_info["avg_t_min_tdew"]: "Difference between current and dew-point temperatures"}
                fits_info["HUMIDIT"] = {weather_info["avg_hum"]: "The average humidity"}
                fits_info["RELSKYT"] = {weather_info["avg_cloud"]: "The relative sky temperature (average cloud cover)"}
                fits_info["WIND"] = {weather_info["avg_wind"]: "The average wind speed"}
                fits_info["ENVTEM"] = {weather_info["avg_temp"]: "The average ambient temperature"}
                fits_info["SEEING"] = {weather_info["seeing"]: "The curent seeing"}
        except Exception as e:
            log.warn("Unable to get weather and seeing information: {}".format(e))

    def get_timing_info(self, fits_info):
        fits_info["DATE-OBS"] = {datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f"): "The time the user pushed start (UTC)"}
        if self.camc.state["trigger_mode"] == 0:
            fits_info["GPSSTART"] = {"": "GPS start time (UTC; external)"}
            fits_info["GPS-INT"] = {"": "GPS trigger interval (msec)"}
        elif self.camc.state["trigger_mode"] in [1, 6]:
            if self.gpsc is None:
                raise Exception("The mode is set to external trigger or external start, but the GPS appears to be missing.")

            # Get the GPS date, time, etc.
            # The pop date is stored by the TM4 as MMDDYYYY, 
            # and the time as HHMMSS.SSSSSSS
            gpss = self.gpsc.get_status()
            pop_state = gpss['pop_state']
            pop_date = gpss['pop']['date']
            pop_time = gpss['pop']['time']
            pop_frequency = gpss['pop']['frequency']
            if pop_state in [1, 2]:
                start_date_time = str(pop_date) + "T" + pop_time
                # Strip off the floating point part
                start_date_time, frac = start_date_time.split('.')
                date_time_obj = datetime.strptime(start_date_time, "%m%d%YT%H%M%S")
                start_date_time = date_time_obj.strftime("%Y-%m-%dT%H:%M:%S")
                start_date_time = "{}.{}".format(start_date_time, frac)
                fits_info["GPSSTART"] = {start_date_time: "GPS start time (UTC; external)"}
                if self.camc.state["trigger_mode"] == 1:
                    fits_info["GPS-INT"] = {str(pop_frequency): "GPS trigger interval (msec)"} # External trigger
                else:
                    fits_info["GPS-INT"] = {"": "GPS trigger interval (msec)"} # External start
            else:
                raise Exception("The POP state is not pending")
        else:
            raise Exception("Invalid trigger mode. We shouldn't ever see this")
                

if __name__ == '__main__':
    print("Instantiating  parent controller")
    try:
        controller = Controller()
    except:
        print("Exception while instantiating controller")
        sys.exit()
    try:
        print("Getting statuses")
        print("")
        print("Filterwheel status: {}".format(controller.fwc.get_status()))
        print("")
        print("GPS status: {}".format(controller.gps.get_status()))
        print("")
    except:
        print("Caught an exception getting statuses")
    try:
        resp = input("Set the GPS to trigger every 0.5 seconds. Push 'y' when done: ")
        if resp.upper() != 'Y':
            print("Wrong response. Try again")
            sys.exit()
        print("Initializing camera")
        controller.camc.initialize()
        print("Getting bits and bobs of info")
        controller.camc.set_acquisition_mode(3)
        controller.camc.set_trigger_mode(1)
        controller.camc.set_readout_mode(4)
        controller.camc.set_exposure_time(0.4)
        controller.camc.set_kinetic_series_length(20)
        print("Taking exposure")
        controller.start_exposure()
        time.sleep(15)
    except Exception as e:
        print("Uh oh. Something went wrong")
        print(str(e))
    while(True):
        try:
            controller.camc.shutdown()
            break
        except Exception as e:
            print("Exception caught while shutting down: {}".format(str(e)))
            time.sleep(10)
    print("...and we're done")
