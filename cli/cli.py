#!/usr/bin/env python
"""WiNCam command line client."""

import cmd
import sys
import socket

from itertools import count
from functools import wraps

def throws(*exceptions):
    """Handles common exceptions."""

    def wrapper(f):
        @wraps(f)
        def wrapped(self, *args, **kwargs):
            try:
                if exceptions:
                    try:
                        return f(self, *args, **kwargs)
                    except (exceptions) as e:
                        print('System error: {}'.format(e.message))
                else:
                    return f(self, *args, **kwargs)
            except socket.timeout:
                print('Connection timed out.')
            except socket.error as e:
                print('Network error: {}'.format(e))
        return wrapped
    return wrapper


class BaseCLI(cmd.Cmd):
    # We'll use this as a lookup in the `default` method.
    CMD_MAP = {}

    def __init__(self, client=None):
        # Using super() doesn't seem to work on Cmd objects so we invoke the
        # default __init__ like this instead.
        cmd.Cmd.__init__(self)
        if client is not None:
            self.c = client
        self.do_help(None)
        
    def default(self, line):
        command = line.split()[0].lower()
        if command == 's':
            self.do_status(line)
        elif command == 'q':
            return True
        elif command in self.CMD_MAP and hasattr(self, self.CMD_MAP[command]):
            getattr(self, self.CMD_MAP[command])(line)
        else:
            cmd.Cmd.default(self, line)

    def emptyline(self):
        """Do nothing when an empty line is entered.

        The default behaviour is to repeat the last command and that's not what
        we want.
        """
        pass

    def do_help(self, line):
        if not line:
            print(self.__doc__)
        else:
            cmd.Cmd.do_help(self, line)

    def do_quit(self, line):
        """Exit the program."""
        return True

    def do_EOF(self, line):
        return True


