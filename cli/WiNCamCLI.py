#!/usr/bin/env python
"""WiNCam instrument command line client"""

from __future__ import print_function
import sys
import argparse
from shutil import copyfile
import logging as log

from wincam import __version__
import cli
from controller.TopLevelController import Controller
from wincam import detector, filter, shutter
from wincam.interface.ttypes import WincamException
from wincam.detector.interface.ttypes import WincamDetectorException
from wincam.shutter.interface.ttypes import WincamShutterException
from wincam.filter.interface.ttypes import WincamFilterException
from wincam.cryostat.interface.ttypes import WincamCryostatException

from datetime import datetime
import time
import json

try:
    from pyds9 import DS9
except ImportError:
    from ds9 import ds9 as DS9

from astropy.io import fits


amplifier_mappings = {
    1: "Upper left (one output)",
    2: "Upper right (one output)",
    3: "Lower right (one output)",
    4: "Lower left  (one output)",
    5: "Upper left and Lower right (two outputs)",
    6: "Upper right and Lower left (two outputs)",
    7: "Lower (two outputs)",
    8: "Upper (two outputs)",
    9: "All   (four outputs)",
}


class CLI(cli.BaseCLI):
    """WiNCam command line client.

    System controls:        	    Detector controls:
                                        enable_shutter
        power_on		    	exposure_time
        power_off		    	num_exposures
        reset			    	binning
                                        binning_use
                                        region
    Combination controls:	        region_use
        set_frame_transfer		select_amps
        set_full_frame			gain
                                        readout_speed
    Filter controls:			change_bias
        initialize_filters		write_config
        scan_filters			start_exposure
        change_filter			start_continuous
                                        abort_exposure
                                        validate
                                        observation_type
    Shutter controls:                   amplifier_policy
        shutter_mode


    Exposure information:	    Special routines:
        exposure_status			start_many
        data_available			ptc
        show_image
        show_fits


    System information:		    Scripting:
        instrument_status		run_script
        weather_status			abort_script
        telescope_status                script_status
        system_health

    quit			    help

    For help with a specific command type 'help <command>'.
    """
    prompt = 'WiNCam >> '

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_power_on(self, line=None):
        """Switch the power on"""
        try:
            self.c.instrument.detector.power_on()
            print("Detector was powered on")
        except Exception as e:
            print("Exception caught powering on: {}".format(e))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_power_off(self, line=None):
        """Switch the power off"""
        try:
            self.c.instrument.detector.power_off()
            print("Detector was powered off")
        except Exception as e:
            print("Exception caught powering off: {}".format(e))

    @cli.throws(shutter.ttypes.WincamShutterException)
    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_set_frame_transfer(self, line=None):
        """Put the system into frame transfer mode"""
        try:
            pos = raw_input(
                "Move frame open at top (A), botttom (B) or centre (C): ")
        except Exception as e:
            print("Exception caught getting frame position: ", e)
            pos = 'A'
        try:
            self.c.instrument.set_frame_transfer(pos)
        except Exception as e:
            print("Exception caught setting frame transfer mode: {}".format(e))

    @cli.throws(shutter.ttypes.WincamShutterException)
    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_set_full_frame(self, line=None):
        """Put the system into full frame mode"""
        try:
            self.c.instrument.set_full_frame()
        except Exception as e:
            print("Exception caught setting full frame mode: {}".format(e))

    @cli.throws(shutter.ttypes.WincamShutterException)
    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_enable_shutter(self, line=None):
        """Enable/disable the shutter"""
        try:
            enable = int(raw_input(
                'Enable (1) or disable (0) the shutter [1]: '))
        except ValueError:
            enable = 1
        self.c.instrument.detector.set_shutter_enabled(enable)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_exposure_time(self, line=None):
        """Set the exposure time."""
        # Prompt for the time
        try:
            time = float(raw_input('Exposure time: '))
        except ValueError:
            print("Must enter a valid time")
            return
        self.c.instrument.detector.set_exposure_time(time)
        state = self.c.instrument.detector.get_state()
        set_time = state.exposure_time
        print("Set exposure time to {}".format(set_time))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_num_exposures(self, line=None):
        """Set the number of exposures."""
        # Prompt for the number
        try:
            num = int(raw_input('Number of exposures: '))
        except ValueError:
            print("Must enter a valid number")
            return
        self.c.instrument.detector.set_num_exposures(num)
        print("Set number of exposures to {}".format(num))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_binning(self, line=None):
        """Set the binning"""
        try:
            xbin = int(raw_input('Horizontal binning factor [1]: '))
        except ValueError:
            print("Must enter a valid integer between 1 and 9. Using 1")
            xbin = 1
        try:
            ybin = int(raw_input("Vertical binning factor [1]: "))
        except ValueError:
            print("Must enter a valid integer between 1 and 9. Using 1")
            ybin = 1
        self.c.instrument.detector.set_binning(xbin, ybin)
        print("Set binning to [{},{}]".format(xbin, ybin))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_region(self, line=None):
        """Set the region.

Note that the region specified by the user may be modified slightly to
conform to IDSAC-allowed regions."""

        try:
            x1 = int(raw_input('Region bottom left x: '))
        except ValueError:
            print("Must enter a valid integer. Using 0")
            x1 = 1
        try:
            y1 = int(raw_input('Region bottom left y: '))
        except ValueError:
            print("Must enter a valid integer. Using 0")
            y1 = 1
        try:
            x2 = int(raw_input("Region top right x: "))
        except ValueError:
            print("Must enter a valid integer. Using 1024")
            x2 = 1024
        try:
            y2 = int(raw_input("Region top right y: "))
        except ValueError:
            print("Must enter a valid integer. Using 1024")
            y2 = 1024
        try:
            self.c.instrument.detector.set_region(x1, y1, x2, y2)
            print("Set region to [{},{}, {},{}]".format(x1, y1, x2, y2))
        except Exception as e:
            print("Error setting region: {}".format(e))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_binning_use(self, line=None):
        """Set the binning usage"""
        use_binning = False
        try:
            use = raw_input("Use binning [y|N]: ")
        except Exception:
            print("Enter y(es) or n(o). Not changing anything...")
            return
        if use.upper() == 'Y':
            print("Setting binning usage to \"true\"")
            use_binning = True
        elif use.upper() == 'N':
            print("Setting binning usage to \"false\"")
            use_binning = False
        else:
            print("Unrecognised option: {}".format(use))
            return
        try:
            self.c.instrument.detector.set_binning_use(use_binning)
        except Exception as e:
            print("Could not set binning usage: {}".format(e))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_region_use(self, line=None):
        """Set the region usage"""
        use_region = False
        try:
            use = raw_input("Use the region [y|N]: ")
        except Exception:
            print("Enter y(es) or n(o). Not changing anything...")
            return
        if use.upper() == 'Y':
            print("Setting region usage to \"true\"")
            use_region = True
        elif use.upper() == 'N':
            print("Setting region usage to \"false\"")
            use_region = False
        else:
            print("Unrecognised option: {}".format(use))
            return
        try:
            self.c.instrument.detector.set_region_use(use_region)
        except Exception as e:
            print("Could not set region usage: {}".format(e))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_select_amps(self, line=None):
        """Select the outputs amps used"""
        default_output_amps = 9
        print("""Output amplifier codes:""")
        for k, v in amplifier_mappings.items():
            print("    {}: {}".format(k, v))

        try:
            amps = int(raw_input("Select the output amps to use [1-9]: "))
            if amps:
                amps = int(amps)
            else:
                print("No value entered. Using default value {}"
                      .format(default_output_amps))
                amps = default_output_amps
        except ValueError as e:
            print("{}> Must enter a valid integer. Using {}"
                  .format(e, default_output_amps))
            amps = default_output_amps
        if amps not in range(1, 10):
            print("Must enter an integer in the range 1 - 9")
            amps = default_output_amps
        try:
            self.c.instrument.detector.select_amplifiers(amps)
        except Exception as e:
            print("Could not select output amps: {}".format(e))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_gain(self, line=None):
        """Set the gain index"""
        default_gain_idx = detector.ttypes.Gain.BRIGHT
        for k, v in detector.constants.GAINS.iteritems():
            print("Gain: {}	Index = {}".format(v, k))
        print("Valid indices: {}".format(detector.constants.GAINS.keys()))
        print("")
        try:
            gain_idx = int(raw_input("Gain index [{}]: "
                                     .format(default_gain_idx)))
            if gain_idx:
                gain_idx = int(gain_idx)
            else:
                print("No value entered. Using {}".format(default_gain_idx))
                gain_idx = default_gain_idx
        except ValueError as e:
            print("{}. Must enter a valid integer. Using {}"
                  .format(e, default_gain_idx))
            gain_idx = default_gain_idx
        if str(gain_idx) not in detector.constants.GAINS:
            print("Gain index must be a valid integer (0 or 3)")
            gain_idx = default_gain_idx
        self.c.instrument.detector.set_gain_index(gain_idx)
        print("Set gain index to {}".format(gain_idx))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_readout_speed(self, line=None):
        """Set the readout speed index"""
        default_speed_idx = detector.ttypes.ReadoutSpeed.QUITE_FAST
        for k, v in detector.constants.READOUT_SPEEDS.iteritems():
            print("Speed: {} Index = {}".format(v, k))
        print("")
        try:
            speed_idx = raw_input("Readout speed index [{}]: "
                                  .format(default_speed_idx))
            if len(speed_idx) > 0:
                speed_idx = int(speed_idx)
            else:
                print("No value entered. Using {}".format(default_speed_idx))
                speed_idx = default_speed_idx
        except ValueError as e:
            print("{}. Must enter a valid integer. Using {}"
                  .format(e, default_speed_idx))
            speed_idx = default_speed_idx
        if speed_idx not in detector.constants.READOUT_SPEEDS:
            print("Speed index must be an integer. Using {}"
                  .format(default_speed_idx))
            speed_idx = default_speed_idx
        self.c.instrument.detector.set_readout_speed_index(speed_idx)
        print("Set readout speed index to {}".format(speed_idx))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_observation_type(self, line=None):
        """ Set the observation type"""
        try:
            observation_type = int(raw_input(
                "Enter the image type \
[0 (BIAS)|1 (OBJECT)| 2 (FLAT)| 3 (DARK)]: "
            ))
        except ValueError as e:
            print("Error entering image type: {}".format(e))
        if observation_type in range(5):
            self.c.instrument.detector.set_observation_type(observation_type)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_amplifier_policy(self, line=None):
        """ Set the amplifier policy"""
        try:
            amplifier_policy = int(raw_input(
                "Enter the amplifier policy \
                [0 (SPEED) |1 (CONSISTENCY)]: "
            ))
        except ValueError as e:
            print("Error entering amplifier policy: {}".format(e))
        self.c.instrument.detector.set_amplifier_policy(amplifier_policy)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_change_bias(self, line=None):
        """Change the bias level"""
        try:
            idx = int(raw_input('Bias level [1|2]: '))
        except ValueError:
            print("Enter 1 or 2")
            return
        if idx not in [1, 2]:
            print("Enter 1 or 2")
            return
        self.c.instrument.detector.change_bias(idx)
        print("Changed bias to level {}".format(idx))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_set_scale_factor(self, line=None):
        """Setting scale factor"""
        try:
            scale_factor = int(raw_input('Scale factor [1]: '))
        except ValueError:
            print("Must enter an integer >= 1. Using 1")
            scale_factor = 1
        self.c.instrument.detector.set_scale_factor(scale_factor)
        print("Set scale factor to {}".format(scale_factor))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_scale_image(self, line=None):
        """Scale the image as set by the scale factor"""
        self.c.instrument.detector.scale_image()
        print("Image was scaled")

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_write_config(self, line=None):
        """Writing config file to IDSAC controller"""
        self.c.instrument.detector.write_parameters()
        print("Wrote config")

    def do_validate(self, line=None):
        """Check that the parameters to be passed to the IDSAC are valid"""
        valid, message = self.c.instrument.detector.are_parameters_valid()
        if valid:
            print("Parameters are valid")
        else:
            print("Parameters are NOT valid: {}".format(message))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def wait_for_exposure_finish(self, init_sleep, sleeptime):
        time.sleep(init_sleep)
        elapsed = 0
        while True:
            state = self.c.instrument.detector.get_state()
            exp_state = state.exposure_state
            state_description = self.exposure_state_description(exp_state)
            sys.stdout.write("\033[K")
            print("Exposure state = {}    Elapsed time = {:03d}\r"
                  .format(state_description, elapsed), end='\r')
            sys.stdout.flush()
            if exp_state == 0:
                break
            time.sleep(sleeptime)
            elapsed += sleeptime
        print("")
        time.sleep(1)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_start_many(self, line=None):
        """Start one or more sequential exposure sequences,\
displaying progress"""
        print("""This allows a user to specify one or more exposure sequences.
The progress of each exposure is monitored and displayed on-screen""")
        try:
            num = int(raw_input('Number of exposure sequences [1]: '))
        except ValueError:
            print("Must enter a valid integer between 1 and a lot. Using 1")
            num = 1
        sleeptime = 1
        init_sleep = 1
        num_in_sequence = self.c.instrument.detector.get_state().num_exposures
        for i in range(num):
            print("")
            print("Doing exposure sequence {} of {}".format(i+1, num))
            for s in range(num_in_sequence):
                print("Doing exposure {} of {}".format(s+1, num_in_sequence))
                try:
                    fits_info = self.c.gather_fits_info()
                except Exception as e:
                    print("Exception caught gathering fits info: {}".format(e))
                    return
                try:
                    self.run_start_exposure(fits_info)
                except Exception as e:
                    print("Caught exception: {}".format(e))
                    break
                print("Exposure {} started. Waiting for it to finish"
                      .format(s))
                self.wait_for_exposure_finish(init_sleep, sleeptime)
                time.sleep(1)
                print("")
                print("Done exposure {}".format(s+1))

            print("Done exposure sequence {}".format(i+1))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_start_continuous(self, line=None):
        """Start continuous exposures"""
        try:
            self.c.start_exposure(fits_info={}, continuous=True)
            print("Continuous exposure started")
        except Exception as e:
            print("Error starting continuous mode: {}".format(e))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_start_exposure(self, line=None):
        """Start the exposure"""
        try:
            self.run_start_exposure()
            print("Exposure started")
        except Exception as e:
            print("Exception caught starting exposure: {}".format(e))
            return

    @cli.throws(detector.ttypes.WincamDetectorException)
    def run_start_exposure(self, fits_info={}):
        self.c.start_exposure(fits_info)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_abort_exposure(self, line=None):
        try:
            self.c.aborted = True
            retval = self.c.instrument.abort_exposure()
            if retval == 0:
                print("Exposure aborted successfully")
            elif retval == -1:
                print("No action: abort called while not powered on")
            elif retval == -2:
                print("No action: abort called in IDLE or setup state")
            elif retval == -3:
                print("Abort called in READOUT state.\
 Readout will be completed")
        except Exception as e:
            print("Exception caught aborting: {}".format(e))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_reset(self, line=None):
        self.c.instrument.detector.reset()
        print("System reset")

    @cli.throws(detector.ttypes.WincamDetectorException)
    def exposure_state_description(self, exp_state):
        state_description = "Idle"
        if exp_state == 0:
            state_description = "Idle"
        if exp_state == 1:
            state_description = "Writing config"
        if exp_state == 2:
            state_description = "Exposing"
        if exp_state == 3:
            state_description = "Readout"
        return state_description

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_exposure_status(self, line=None):
        exp_state = int(self.c.instrument.detector.get_state().exposure_state)
        state_description = self.exposure_state_description(exp_state)
        print("Exposure status is {}: {}".format(exp_state, state_description))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_data_available(self, line=None):
        data_available = self.c.instrument.detector.data_available()
        if data_available:
            print("Data is available")
        else:
            print("Data is not available")

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_show_image(self, line=None):
        filename = "tmp.fits"
        print("Generating fits...")
        print("Getting data...")
        arr = self.c.instrument.detector.get_image_data()
        print("Got array size {}".format(len(arr)))
        print("Generating fits with keys...")
        self.gen_fits(arr, filename)
        print("Displaying {}".format(filename))
        d = DS9('ds9')
        d.set("file {}".format(filename))

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_show_fits(self, line=None):
        filename = "tmp.fits.gz"
        print("Getting fits...")
        fits_img = self.c.instrument.detector.get_gzipped_fits_image()
        if fits_img:
            with open(filename, 'w') as f:
                f.write(fits_img)
            print("Displaying {}".format(filename))
            d = DS9('ds9')
            d.set("file {}".format(filename))
        else:
            print("There was no image to show")

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_show_fresh_fits(self, line=None):
        filename = "tmp.fits.gz"
        print("Getting fits...")
        fits_img = self.c.instrument.detector.get_fresh_fits_image()
        if fits_img:
            with open(filename, 'w') as f:
                f.write(fits_img)
            print("Displaying {}".format(filename))
            d = DS9('ds9')
            d.set("file {}".format(filename))
        else:
            print("There was no fresh image to show")

    @cli.throws(filter.ttypes.WincamFilterException)
    def do_initialize_filters(self, line=None):
        """Initializes the filter system."""

        self.c.instrument.filter.initialize()

    @cli.throws(filter.ttypes.WincamFilterException)
    def do_scan_filters(self, line=None):
        """Scan the filter IDs."""

        self.c.instrument.filter.scan()

    @cli.throws(filter.ttypes.WincamFilterException)
    def do_change_filter(self, line=None):
        """Select a new filter."""

        status = self.c.instrument.filter.get_status()

        if not status.filters:
            print('Please run the scan procedure before selecting a filter.')
        else:
            options = {}

            print('')

            for i, f in enumerate(status.filters, start=1):
                options[i] = f
                print('    {}. {}'.format(i, status.filters[f]))

            print('')
            print('    Press <Enter> to cancel.')
            print('')

            while True:
                try:
                    option = int(raw_input('Please select a filter: '))
                except ValueError:
                    return

                if option in options.keys():
                    break

            self.c.instrument.filter.change_filter(options[option])

    def do_instrument_status(self, line=None):
        print("")
        self.display_detector_state()
        print("")
        self.display_shutter_state()
        print("")
        self.display_filter_state()
        print("")
        print("="*80)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def display_detector_state(self):
        try:
            state = self.c.state().instrument_state.detector_state
        except (WincamDetectorException, WincamFilterException, WincamShutterException, WincamCryostatException) as e:
            print("Exception caught getting state: {}".format(e))
            return
        print("="*15, " Detector State ", "="*49)
        print("""
    Exposure time (s).......{:<7.3f}		Number of exposures.....{:<3d}
    Readout speed...........{}	       	Gain....................{}"""
              .format(state.exposure_time, state.num_exposures,
                      detector.ttypes.ReadoutSpeed.
                      _VALUES_TO_NAMES[state.readout_speed_index],
                      detector.ttypes.Gain._VALUES_TO_NAMES[state.gain_index]))
        if state.binning_enabled:
            print("""    Binning.................Enabled
    Bin X...................{:0>3d}			Bin Y...................{:0>3d}"""
                  .format(state.xbin, state.ybin))
        else:
            print("""    Binning.................Disabled""")

        if state.region_enabled:
            print("""    Regions.................Enabled
    User entered region.....({:0>4d}, {:0>4d}) ({:0>4d}, {:0>4d})
    Calculated region.......({:0>4d}, {:0>4d}) ({:0>4d}, {:0>4d})"""
                  .format(state.user_x1, state.user_y1,
                          state.user_x2, state.user_y2,
                          state.roi_x1, state.roi_y1,
                          state.roi_x2, state.roi_y2))
        else:
            print("""    Regions.................Disabled""")
        print("""    Observation type........{}"""
              .format(detector.ttypes.ObservationType
                      ._VALUES_TO_NAMES[state.observation_type]))
        print("""    Detector type...........{}			Shutter.................{}"""
              .format(detector.ttypes.DetectorType
                      ._VALUES_TO_NAMES[state.detector_type],
                      "Enabled" if state.shutter_enabled else "Disabled"))
        if (state.detector_type == detector.ttypes.DetectorType.FTCCD):
            print("""    Frame Transfer Region...{}"""
                  .format(detector.ttypes.FTRegion
                          ._VALUES_TO_NAMES[state.ft_region]))
        print("""    Amplifiers..............{:<3d}			Number of samples.......{:<3d}
    Exposure State..........{}		Exposure sequence num...{}
    Exposure Type...........{}
    Data available..........{}		Data fresh..............{}
    Powered on..............{}		Current file............{}
    Disk Space .............{}		Disk space free.........{}"""
              .format(
                  state.amplifiers, state.num_samples,
                  detector.ttypes.ExposureState
                  ._VALUES_TO_NAMES[state.exposure_state],
                  state.exposure_sequence_number,
                  detector.ttypes.ExposureType
                  ._VALUES_TO_NAMES[state.exposure_type],
                  "True" if state.data_available else "False",
                  "True" if state.data_fresh else "False",
                  "True" if state.powered_on else "False",
                  state.current_file,
                  state.disk_space, state.disk_space_free))

    @cli.throws(shutter.ttypes.WincamShutterException)
    def display_shutter_state(self):
        print("="*15, " Shutter State ", "="*36)
        try:
            state = self.c.instrument.shutter.get_status()
        except Exception as e:
            print("Unable to display shutter state: {}".format(e))
            return
        print("""
    Shutter Ready ..........{:<6s}              Mode....................{:<3d}
    Position A .............{:<6d}		Position B.....{:<6d}""".format
              (
                  str(self.c.instrument.shutter.is_ready()),
                  state.mode,
                  state.position_a,
                  state.position_b
              )
              )

    @cli.throws(filter.ttypes.WincamFilterException)
    def display_filter_state(self):
        try:
            state = self.c.instrument.filter.get_status()
        except filter.ttypes.WincamFilterException as e:
            msg = "Could not display filter state: {}".format(e)
            log.error(msg)
            print(msg)
            return

        print("="*15, " Filter State ", "="*50)
        print("""    Filter ready ...........{}		Filter initialized .....{}
    Current filter .........{:<3d}
    Available filters:"""
              .format("True" if state.is_ready else "False",
                      "True" if state.is_initialized else "False",
                      state.current_filter))
        for (k, v) in state.filters.items():
            print("        {}: {}".format(k, v))

    def do_weather_status(self, line=None):

        try:
            info = self.c.weather.get_weather_info()
        except Exception as e:
            msg = "Error getting weather info: {}".format(e)
            print(msg)
            log.error(msg)
            return

        print("="*15, " Current weather ", "="*47)
        print(
            """    === Values ===
    Seeing......{:5.1f}	Wind......{:5.1f}
    Temperature.......{:5.1f}	T-Tdew.......{:5.1f}
    Humidity..........{:5.1f}	Cloud........{:5.1f}
    === Warnings ===
    Temperature........{}	T-Tdew.......{}
    Humidity...........{}       Wind.........{}
    Cloud..............{}       Rain.........{}"""
            .format(
                float(info['seeing']), float(info['avg_wind']),
                float(info['avg_temp']), float(info['avg_t_min_tdew']),
                float(info['avg_hum']), float(info['avg_cloud']),
                "Yes" if int(info['temp_warn']) else "No",
                "Yes" if int(info['t_min_tdew_warn']) else "No",
                "Yes" if int(info['hum_warn']) else "No",
                "Yes" if int(info['wind_warn']) else "No",
                "Yes" if int(info['cloud_warn']) else "No",
                "Yes" if int(info['rain_warn']) else "No"
            )
        )

    def do_telescope_status(self, line=None):
        print("Current telescope status:")
        try:
            status = self.c.tcs.get_status()
        except Exception as e:
            print("Error getting TCS state: {}".format(e))
            return
        print("RA = {}".format(status["tel_ra"]))
        print("Dec = {}".format(status["tel_dec"]))
        print("Alt = {}".format(status["tel_alt"]))
        print("Az = {}".format(status["tel_az"]))

    def gen_fits(self, arr, filename):
        """Generate a simple fits file.

        Only the basic data is included (no extra keywords).
        """
        hdu = fits.PrimaryHDU()
        hdu.data = arr
        hdu.writeto(filename, overwrite=True)

    def start_of_night(self):
        date = datetime.now()
        y = int(date.strftime("%Y"))
        m = int(date.strftime("%m"))
        d = int(date.strftime("%d"))
        h = int(date.strftime("%H"))
        min = int(date.strftime("%M"))
        s = int(date.strftime("%s"))
        print("The hour is {}".format(h))
        if h < 12.0:
            print("It's before 12: use the previous day")
            if m == 1 and d == 1:
                print("January 1 before 12: use 31/12 of the previous year")
                y = y - 1
                d = 31
                m = 12
            elif d == 1:
                print("First of the month: use the last of the previous month")
                m = m - 1
                if m in [1, 3, 5, 7, 8, 10, 12]:
                    d = 31
                elif m in [4, 6, 9, 11]:
                    d = 30
                elif m == 2:
                    if y % 4 == 0:
                        d = 29
                    else:
                        d = 29
                else:
                    print("Invalid month")
                    sys.exit(1)
            else:
                d = d - 1
        print("Start of night = {} {} {} {} {} {}".format(y, m, d, h, min, s))
        return (y, m, d, h, min, s)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_ptc(self, line=None):
        import os
        from os.path import expanduser
        homedir = expanduser("~")
        (year, month, day, hour, minute, sec) = self.start_of_night()
        file_dir = "{}/data/1.0m/wincam/{}/{:02d}{:02d}".format(
            homedir, year, int(month), int(day))
        print("Using input file dir = {}".format(file_dir))
        outpath = "{}/data/ptc/{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}".format(
            homedir, int(year), month, day, hour, minute, int(sec))
        if not os.path.exists(outpath):
            os.makedirs(outpath)
        try:
            num_sets = int(raw_input("Enter the numer of PTC sets: "))
        except ValueError:
            print("Number of sets should be an integer")
            return
        try:
            initial_time = float(raw_input("Enter the initial PTC time: "))
        except ValueError:
            print("Initial time should be a float number")
            return
        try:
            time_step = float(raw_input("Enter the PTC time step: "))
        except ValueError:
            print("The PTC time step should be a float number")
            return
        # Run the PTC options
        init_sleep = 1
        sleeptime = 1
        # Copy the parameter template file to the output directory
        parameter_file = "{}/programming/IDSAC_V2_API/parameter/template.txt"\
                         .format(homedir)
        output_file = "{}/ParameterFile.txt".format(outpath)
        copyfile(parameter_file, output_file)
        fits_info = {}
        for AB in ["A", "B"]:
            exp_time = 0.001
            fits_info["PTCNUM"] = {"{}".format(num_sets):
                                   "The number of exposures in the PTC set"}
            fits_info["PTCSEQ"] = {"{}".format(0):
                                   "This exposure in the PTC set"}
            fits_info["PTCTIME"] = {"{}".format(exp_time):
                                    "The exposure time of this PTC exposure"}
            fits_info["PTCINT"] = {"{}".format(time_step):
                                   "The time interval in the PTC sequence"}
            fits_info["PTCSET"] = {"{}".format(AB): "The PTC set"}
            self.c.instrument.detector.set_shutter_enabled(0)
            self.c.instrument.detector.set_exposure_time(exp_time)
            print("Starting {} second exposure".format(exp_time))
            self.run_start_exposure(fits_info)
            self.wait_for_exposure_finish(init_sleep, sleeptime)
            file_name = self.c.instrument.detector.get_save_name()
            full_file = "{}/{}".format(file_dir, file_name)
            self.copy_file(full_file, outpath, AB, exp_time)
            self.c.instrument.detector.set_shutter_enabled(1)
            exp_time = initial_time
            for i in range(num_sets):
                self.c.instrument.detector.set_exposure_time(exp_time)
                fits_info["PTCSEQ"] = {"{}".format(i+1):
                                       "This exposure in the PTC set"}
                fits_info["PTCTIME"] = {"{}".format(exp_time):
                                        "Exposure time of this PTC exposure"}
                print("Starting {} second exposure".format(exp_time))
                self.run_start_exposure(fits_info)
                self.wait_for_exposure_finish(init_sleep, sleeptime)
                file_name = self.c.instrument.detector.get_save_name()
                full_file = "{}/{}".format(file_dir, file_name)
                self.copy_file(full_file, outpath, AB, exp_time)
                exp_time = exp_time + time_step
        self.c.instrument.detector.set_shutter_enabled(0)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_run_script(self, line=None):
        scriptfile = raw_input("Enter the script name: ")
        print("You entered ", scriptfile)
        if len(scriptfile) == 0 or not scriptfile.endswith(".json"):
            print("Invalid script name")
            return

        try:
            iters = int(raw_input(
                "Enter the number of iterations of the script: "))
        except Exception as e:
            print("Invalid input: {}. Using iterations = 1".format(e))
            iters = 1

        try:
            with open(scriptfile) as f:
                script = json.load(f)
        except IOError as e:
            print("Unable to open file {}: {}".format(scriptfile, e))
            return
        except ValueError as e:
            print("Invalid json file {}: {}".format(scriptfile, e))
            return
        self.c.run_script(script, iters)

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_abort_script(self, line=None):
        self.c.abort_script()

    @cli.throws(detector.ttypes.WincamDetectorException)
    def do_script_status(self, line=None):
        script_info = self.c.script_info
        active = "ACTIVE" if script_info["active"] else "INACTIVE"
        iteration = script_info["current_iteration"] + 1
        num_its = script_info["total_iterations"]
        current_unit = script_info["script_unit_count"] + 1
        num_units = script_info["total_units"]
        print("="*15, " Script Status ", "="*50)
        print(" "*80)
        print("    Script is {}".format(active))
        print(" ")
        print("    Doing iteration      {:3d} of {:3d}"
              .format(iteration, num_its))
        print(" ")
        print("    Running script unit  {:3d} of {:3d}"
              .format(current_unit, num_units))
        if "comment" in script_info.keys() and script_info["comment"] is not None:
            print("    Unit description      {}".format(
                script_info["comment"]))
        print(" ")
        print("="*80)

    def copy_file(self, full_file, outpath, exptime, AB):
        copyfile(full_file, "{}/{}{}.fits".format(outpath, AB, exptime))


def main():
    parser = argparse.ArgumentParser(description=__doc__,
                                     prog='wincam-detector-cli')
    parser.add_argument('--version', action='version',
                        version='%(prog)s {}'.format(__version__))

    parser.add_argument('--tcshost', default='localhost',
                        help='the host on which the TCS runs')

    parser.add_argument(
        '--weatherhost', default='localhost',
        help='the host on which the weather monitoring service runs')

    parser.add_argument('--instrumenthost', default='localhost',
                        help='the host on which runs')

    parser.add_argument('--log',
                        help="Specify the logging level.", default='info',
                        choices=['debug', 'info', 'warn', 'error', 'critical'])
    parser.add_argument('--logfile',
                        help="Specify the log file.")

    arguments = parser.parse_args()

    setup_logging(arguments)

    try:
        client = Controller(
            instrumenthost=arguments.instrumenthost,
            weatherhost=arguments.weatherhost,
            tcshost=arguments.tcshost,
        )
    except Exception as e:
        sys.exit(e)

    intro = 'WiNCam {}\nType "help" for more information.'.format(__version__)

    try:
        CLI(client).cmdloop(intro)
    except KeyboardInterrupt:
        sys.exit(0)


def setup_logging(args):
    # Set up logging
    logfile = __file__.split('.py')[0] + ".log"
    if "logfile" in args and args.logfile is not None:
        logfile = args.logfile
    print("Writing logs to {}".format(logfile))
    loglevel = args.log
    numeric_level = getattr(log, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(loglevel))
    log.basicConfig(
        level=numeric_level,
        filename=logfile,
        format='%(asctime)s [%(levelname)s\t]: %(message)s'
    )


if __name__ == '__main__':
    main()
